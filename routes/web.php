<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Staff\StaffController;
use App\Http\Controllers\Department\DepartmentController;
use App\Http\Controllers\Setka\SetkaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('staffs', StaffController::class);
Route::resource('departments', DepartmentController::class);
Route::get('setka', [SetkaController::class, 'index'])->name('setka.index');

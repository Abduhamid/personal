<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStaffsDepartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs_departments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('staff_id')
                ->constrained()
                ->references('id')
                ->on('staffs');
            $table->foreignId('department_id')
                ->constrained()
                ->references('id')
                ->on('departments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_staffs_depatments');
    }
}

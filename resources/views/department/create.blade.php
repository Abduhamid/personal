@extends('layouts.app')

@section('content')
    <div>
        <div>
            <h3>{{ trans('actions.general.create') }}</h3>
        </div>
        <div>
            @include('vendor.errors.errors')
            {!! Form::open(['route' => 'departments.store','class'=>'form-horizontal','id'=>'form-department']) !!}
                @include('department.partials.fields')
            {!! Form::close() !!}
        </div>
        <div>
            {!!  link_to(URL::previous(), trans('actions.general.cancel'), ['class' => 'btn btn-default']) !!}
            {!! Form::submit(trans('actions.general.save'), ['class' => 'btn btn-primary','form'=>'form-department']) !!}
        </div>
    </div>
@endsection
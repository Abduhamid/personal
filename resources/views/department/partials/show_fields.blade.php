<div class="form-group required">
    {!! Form::label('name', trans('department.name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $department->name }}</p>
    </div>
</div>
@extends('layouts.app')

@section('content')
    <div class="mt-5">
        @include('vendor.errors.success')
        @include('vendor.errors.error')
        <a href="{{route('departments.create')}}" class = 'btn btn-primary'>{{trans('actions.general.create')}}</a>
        <br>
        <br>
        <table class="table">
            <thead>
                <tr class="table-primary">
                    <td>ID</td>
                    <td>{{trans('department.name')}}</td>
                    <td>{{trans('department.count_staff')}}</td>
                    <td>{{trans('department.max_salary')}}</td>
                    <td>{{trans('actions.general.action')}}</td>
                </tr>
            </thead>
            <tbody>
                @foreach($departments as $department)
                    <tr>
                        <td>{{$department->id}}</td>
                        <td>{{$department->name}}</td>
                        <td>{{$department->staffs->count()}}</td>
                        <td>{{$department->staffs->max('salary')}}</td>
                        <td class="text-center">
                            <a href="{{ route('departments.show', $department->id)}}" class="btn btn-success btn-sm">{{trans('actions.general.view')}}</a>
                            <a href="{{ route('departments.edit', $department->id)}}" class="btn btn-success btn-sm">{{trans('actions.general.edit')}}</a>
                            <form action="{{ route('departments.destroy', $department->id)}}" method="post" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit">{{trans('actions.general.delete')}}</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    <div>
    @if(isset($departments))
        {{$departments->render() }}
    @endif
@endsection
@extends('layouts.app')

@section('content')
    <div>
        <div>
            <h3>{{ trans('actions.general.view') }}</h3>
        </div>
        <div>
            @include('department.partials.show_fields')
        </div>
        <div>
            {!!  link_to(URL::previous(), trans('actions.general.back'), ['class' => 'btn btn-default']) !!}
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div>
        <div>
            <h3>{{ trans('actions.general.create') }}</h3>
        </div>
        <div>
            @include('vendor.errors.errors')
            {!! Form::open(['route' => 'staffs.store','class'=>'form-horizontal','id'=>'form-staff']) !!}
            @include('staff.partials.fields')
            {!! Form::close() !!}
        </div>
        <div>
            {!!  link_to(URL::previous(), trans('actions.general.cancel'), ['class' => 'btn btn-default']) !!}
            {!! Form::submit(trans('actions.general.save'), ['class' => 'btn btn-primary','form'=>'form-staff']) !!}
        </div>
    </div>
@endsection
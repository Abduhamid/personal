<div class="form-group required">
    {!! Form::label('first_name', trans('staff.first_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group required">
    {!! Form::label('last_name', trans('staff.last_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group required">
    {!! Form::label('middle_name', trans('staff.middle_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::text('middle_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group required">
    {!! Form::label('gender', trans('staff.gender').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::select('gender', ["-1"=>"",1=>'Мужской', 0=>'Женский'], $staff->gender??null, ['class' => 'form-control col-sm-9']) !!}
    </div>
</div>

<div class="form-group required">
    {!! Form::label('salary', trans('staff.salary').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::number('salary', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group required">
    {!! Form::label('departments', trans('staff.departments').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        {!! Form::select('department_id', $departments, isset($staff->departments)?$staff->departments->pluck('id'):null,
            ['name' => 'department_id[]', 'id' => 'select2_perms',
            'class' => 'form-control select2 ', 'data-placeholder' => 'Выберите отделы', 'multiple']) !!}
    </div>
</div>
<div class="form-group required">
    {!! Form::label('first_name', trans('staff.first_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff['first_name'] }}</p>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('last_name', trans('staff.last_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff['last_name'] }}</p>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('middle_name', trans('staff.middle_name').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff['middle_name'] }}</p>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('gender', trans('staff.gender').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff['genderName'] }}</p>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('salary', trans('staff.salary').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff['salary'] }}</p>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('departments', trans('staff.departments').':', ['class' => 'control-label col-sm-2']) !!}
    <div class='col-sm-9'>
        <p class="form-control-static">{{ $staff->departments->implode('name', ', ')??'' }}</p>
    </div>
</div>
@extends('layouts.app')

@section('content')
    <div class="mt-5">
        @include('vendor.errors.success')
        @include('vendor.errors.error')
        <a href="{{route('staffs.create')}}" class = 'btn btn-primary'>{{trans('actions.general.create')}}</a>
        <br>
        <br>
        <table class="table">
            <thead>
            <tr class="table-primary">
                <td>ID</td>
                <td>{{trans('staff.first_name')}}</td>
                <td>{{trans('staff.last_name')}}</td>
                <td>{{trans('staff.middle_name')}}</td>
                <td>{{trans('staff.gender')}}</td>
                <td>{{trans('staff.salary')}}</td>
                <td>{{trans('staff.departments')}}</td>
                <td>{{trans('actions.general.action')}}</td>
            </tr>
            </thead>
            <tbody>
            @foreach($staffs as $staff)
                <tr>
                    <td>{{$staff->id}}</td>
                    <td>{{$staff->first_name}}</td>
                    <td>{{$staff->last_name}}</td>
                    <td>{{$staff->middle_name}}</td>
                    <td>{{$staff->genderName}}</td>
                    <td>{{$staff->salary}}</td>
                    <td>{{$staff->departments->implode('name', ', ')}}</td>
                    <td class="text-center">
                        <a href="{{ route('staffs.show', $staff->id)}}" class="btn btn-success btn-sm">{{trans('actions.general.view')}}</a>
                        <a href="{{ route('staffs.edit', $staff->id)}}" class="btn btn-success btn-sm">{{trans('actions.general.edit')}}</a>
                        <form action="{{ route('staffs.destroy', $staff->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit">{{trans('actions.general.delete')}}</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div>
    @if(isset($staffs))
        {{$staffs->render() }}
    @endif
@endsection
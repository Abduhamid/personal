@extends('layouts.app')

@section('content')
    <div>
        <div>
            <h3>{{ trans('actions.general.edit') }}</h3>
        </div>
        <div>
            @include('vendor.errors.errors')
            {!! Form::model($staff , ['route' => ['staffs.update', $staff->id],
                'method' => 'patch', 'class'=>'form-horizontal','id'=>'form-staff']) !!}
            @include('staff.partials.fields')
            {!! Form::close() !!}
        </div>
        <div>
            {!!  link_to(URL::previous(), trans('actions.general.cancel'), ['class' => 'btn btn-default']) !!}
            {!! Form::submit(trans('actions.general.save'), ['class' => 'btn btn-primary','form'=>'form-staff']) !!}
        </div>
    </div>
@endsection
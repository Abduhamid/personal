@extends('layouts.app')

@section('content')
    <div class="mt-5">
        <table class="table">
            <thead>
                <tr class="table-primary">
                    <td></td>
                    @foreach($departments as $department)
                        <td>{{$department->name}}</td>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($staffs as $staff)
                    <tr>
                        <td>{{$staff->fullName}}</td>
                        @foreach($departments as $department)
                            <td>{{$staff->departments->contains('id', $department->id)?'+':''}}</td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    <div>
@endsection
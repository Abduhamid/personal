@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>УПС!</strong> {{ trans('alerts.general.someproblems') }}<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

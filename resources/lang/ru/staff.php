<?php

return [
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'middle_name' => 'Отчество',
    'gender' => 'Пол',
    'salary' => 'Заработная плата',
    'departments' => 'Отделы',
];
<?php

namespace App\Providers;

use App\Repositories\Department\DepartmentEloquentRepository;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Staff\StaffEloquentRepository;
use App\Repositories\Staff\StaffRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StaffRepositoryContract::class, StaffEloquentRepository::class);
        $this->app->bind(DepartmentRepositoryContract::class, DepartmentEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

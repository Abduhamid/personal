<?php

namespace App\Http\Controllers\Setka;

use App\Http\Controllers\Controller;
use App\Repositories\Department\DepartmentEloquentRepository;
use App\Repositories\Staff\StaffRepositoryContract;
use Illuminate\Http\Request;

class SetkaController extends Controller
{
    protected $departmentRepository;
    protected $staffRepository;

    public function __construct(StaffRepositoryContract $staffRepository,
                                DepartmentEloquentRepository $departmentRepository
    )
    {
        $this->staffRepository = $staffRepository;
        $this->departmentRepository = $departmentRepository;
    }

    public function index()
    {
        $departments = $this->departmentRepository->all();
        $staffs = $this->staffRepository->all();
        return view('setka.index', compact( 'staffs', 'departments'));
    }
}

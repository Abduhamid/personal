<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\StoreStaffRequest;
use App\Http\Requests\Staff\UpdateStaffRequest;
use App\Repositories\Department\DepartmentEloquentRepository;
use App\Repositories\Staff\StaffRepositoryContract;

class StaffController extends Controller
{
    protected $departmentRepository;
    protected $staffRepository;

    public function __construct(StaffRepositoryContract $staffRepository,
                DepartmentEloquentRepository $departmentRepository
        )
    {
        $this->staffRepository = $staffRepository;
        $this->departmentRepository = $departmentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = $this->staffRepository->paginate();
        return view('staff.index', compact( 'staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = $this->departmentRepository->all()->prepend('', '')->pluck('name', 'id');
        return view('staff.create', compact( 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStaffRequest $request)
    {
        $this->staffRepository->create($request->validated());
        session()->flash('flash_message', trans('alerts.general.success_add'));
        return redirect()->route('staffs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $staff = $this->staffRepository->findById($id);
        return view('staff.show', compact('staff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = $this->staffRepository->findById($id);
        $departments = $this->departmentRepository->all()->prepend('', '')->pluck('name', 'id');
        return view('staff.edit', compact('staff', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStaffRequest $request, $id)
    {
        $this->staffRepository->update($id, $request->validated());
        session()->flash('flash_message', trans('alerts.general.success_edit'));
        return redirect()->route('staffs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->staffRepository->delete($id);
        session()->flash('flash_message', trans('alerts.general.success_delete'));
        return redirect()->route('staffs.index');
    }
}

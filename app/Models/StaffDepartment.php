<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffDepartment extends Model
{
    use HasFactory;

    protected $table = 'staffs_departments';

    protected $fillable = [
        'department_id',
        'staff_id',
    ];
}

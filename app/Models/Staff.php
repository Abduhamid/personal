<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    protected $table = 'staffs';

    protected $genders = [
        1=>'Мужской',
        0=>'Женский'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'gender',
        'salary',
    ];

    public function getGenderNameAttribute()
    {
        return $this->genders[$this->gender]??'';
    }

    public function getFullNameAttribute()
    {
        return "$this->first_name $this->last_name $this->middle_name";
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'staffs_departments');
    }
}

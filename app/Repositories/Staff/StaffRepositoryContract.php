<?php


namespace App\Repositories\Staff;


interface StaffRepositoryContract
{
    public function all();

    public function paginate($perPage = 30);

    public function create(array $data);

    public function update(int $id, array $data);

    public function findById(int $id);

    public function delete(int $id);
}
<?php


namespace App\Repositories\Staff;


use App\Models\Staff;
use App\Repositories\Department\DepartmentRepositoryContract;

class StaffEloquentRepository implements StaffRepositoryContract
{

    protected $staff;
    protected $departmentsRepository;

    public function __construct(Staff $staff, DepartmentRepositoryContract $departmentsRepository)
    {
        $this->staff = $staff;
        $this->departmentsRepository = $departmentsRepository;
    }
    public function all()
    {
        return $this->staff->with('departments')->get();
    }

    public function paginate($perPage = 30)
    {
        $staffs = $this->staff->with('departments')->paginate($perPage);
        return $staffs;
    }

    public function create(array $data)
    {
        $staff = $this->staff->create($data);
        $staff->departments()->sync($data['department_id']);
        return $staff;
    }

    public function update(int $id, array $data)
    {
        $staff = $this->staff->findOrFail($id);
        $staff->update($data);
        $staff->departments()->sync($data['department_id']);
        return $staff;
    }

    public function findById(int $id)
    {
        $staff = $this->staff->with('departments')->findOrFail($id);
        return $staff;
    }

    public function delete(int $id)
    {
        $staff = $this->staff->findOrFail($id);
        $staff->departments()->detach();
        return $staff->delete();
    }
}
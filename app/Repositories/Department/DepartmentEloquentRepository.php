<?php


namespace App\Repositories\Department;


use App\Models\Department;

class DepartmentEloquentRepository implements DepartmentRepositoryContract
{
    protected $department;

    public function __construct(Department $department)
    {
        $this->department = $department;
    }
    public function all()
    {
        return $this->department->get();
    }

    public function paginate($perPage = 30)
    {
        return $this->department->with('staffs')->paginate($perPage);
    }

    public function create(array $data)
    {
        return $this->department->create($data);
    }

    public function update(int $id, array $data)
    {
        $department = $this->findById($id);
        $department->update($data);
        return $department;
    }

    public function findById(int $id)
    {
        return $this->department->findOrFail($id);
    }

    public function delete(int $id)
    {
        $department = $this->findById($id);
        if ($department->staffs->count()>0){
            return false;
        }
        return $department->delete();
    }
}
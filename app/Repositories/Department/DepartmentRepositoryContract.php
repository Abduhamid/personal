<?php


namespace App\Repositories\Department;


interface DepartmentRepositoryContract
{
    public function all();

    public function paginate($perPage = 30);

    public function create(array $data);

    public function update(int $id, array $data);

    public function findById(int $id);

    public function delete(int $id);
}